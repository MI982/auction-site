package auctionhouse.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import org.springframework.beans.factory.annotation.Value;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Builder.Default;
import lombok.Data;

@Entity
@Data
public class User extends ModelTemplate {
	

	private String firstName;
	
	private String lastName;
	
	private String username;
	
	private String password;
	
	private String location;
	
	private String phoneNumber;
	
	@Value("#{1}")
	private int isActive;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Auction> auctions;
	
	@OneToMany(mappedBy = "user", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Bid> bids;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, mappedBy = "user")
	private List<UserRoles> userRoles;
	
	public void addAuction(Auction auction) {
		auctions.add(auction);
		auction.setUser(this);
	}
	
	public void removeAuction(Auction auction) {
		auctions.remove(auction);
		auction.setUser(null);
	}
	
	public void addBid(Bid bid) {
		bids.add(bid);
		bid.setUser(this);
	}
	
	public void removeBid(Bid bid) {
		bids.remove(bid);
		bid.setUser(null);
	}

}
