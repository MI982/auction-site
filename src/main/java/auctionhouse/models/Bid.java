package auctionhouse.models;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.JoinColumns;
import javax.persistence.ManyToOne;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import lombok.Data;

@Entity
@Data
@IdClass(BidKey.class)
public class Bid implements Serializable{
	
	
	private static final long serialVersionUID = 1L;

	@Id
	@ManyToOne
	@JoinColumn(name = "user_id")
	@JsonManagedReference
	private User user;
	
	@Id
	@ManyToOne
	@JoinColumn(name = "auction_id")
	@JsonManagedReference
	private Auction auction;
	
	private Integer bidValue;
	
	private Long bidTime;
	
	public void removeBidFromUserAndAuction() {
		user.removeBid(this);
		auction.removeBid(this);
	}
	
	public void addBidtoUserAndAuction() {
		user.addBid(this);
		auction.addBid(this);
	}
	
	

}
