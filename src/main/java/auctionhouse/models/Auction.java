package auctionhouse.models;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

import lombok.Data;

@Entity
@Data
public class Auction extends ModelTemplate {
	
	
	@JsonManagedReference
	@ManyToOne
	@JoinColumn(name = "user_id", insertable = false, updatable = false)
	private User user;
	
	
	@JsonManagedReference
	@OneToOne
	private Article article;
	
	@NotNull
	private Integer startingPrice;
	
	@NotNull
	private Long createdOn;
	
	@NotNull
	private Long endOn;
	
	@JsonBackReference
	@OneToMany(mappedBy = "auction", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Bid> bids;
	
	@JsonManagedReference
	@ManyToOne
	@JoinColumn(name = "category_id", insertable = false, updatable = false)
	private Category category;
	
	public void removeBid(Bid bid) {
		bids.remove(bid);
		bid.setAuction(null);
	}
	
	public void addBid(Bid bid) {
		bids.add(bid);
		bid.setAuction(this);
	}

}
