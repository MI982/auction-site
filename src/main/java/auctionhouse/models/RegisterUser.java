package auctionhouse.models;

import auctionhouse.customvalidators.RegisterUserValidator;
import auctionhouse.customvalidators.ValidateBy;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ValidateBy(customValidator = RegisterUserValidator.class)
public class RegisterUser {
	
	private String firstName;
	
	private String lastName;
	
	private String username;
	
	private String password;
	
	private String repeatedPassword;
	
	private String location;
	
	private String phoneNumber;

}
