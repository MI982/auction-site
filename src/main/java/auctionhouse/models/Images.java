package auctionhouse.models;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Images extends ModelTemplate {
	
	private String path;
	private String name;
	
	@ManyToOne
	private Article article;

}
