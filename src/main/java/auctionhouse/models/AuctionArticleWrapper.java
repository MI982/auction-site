package auctionhouse.models;

import auctionhouse.customvalidators.AuctionArticleWrapperValidator;
import auctionhouse.customvalidators.ValidateBy;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ValidateBy(customValidator = AuctionArticleWrapperValidator.class)
public class AuctionArticleWrapper {
	
	private String articleName;
	
	private String articleDescription;
	
	private String categoryName;
	
	private Integer auctionLength;
	
	private Integer auctionStartingPrice;
	
	

}
