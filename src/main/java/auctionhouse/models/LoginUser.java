package auctionhouse.models;

import auctionhouse.customvalidators.LoginUserValidator;
import auctionhouse.customvalidators.ValidateBy;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ValidateBy(customValidator = LoginUserValidator.class)
public class LoginUser {
	
	private String username;
	
	private String password;
	
	public String toString() {
		return "username:"+this.getUsername()+", password:"+this.getPassword();
	}

}
