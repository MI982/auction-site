package auctionhouse.models;

import java.io.Serializable;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class BidKey implements Serializable{
	
	private User user;
	private Auction auction;

}
