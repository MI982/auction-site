package auctionhouse.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuctionDTO {

	private Long auctionDTOId;
	
	private String auctionDTOName;
	
	private String auctionDTOUserName;
	
	private Integer auctionDTOStartingBid;
	
	private Long auctionDTOEndOn;
	
	
	public AuctionDTO(Auction auction) {
		
		this.auctionDTOId = auction.getId();
		this.auctionDTOName = auction.getArticle().getName();
		this.auctionDTOUserName = auction.getUser().getUsername();
		this.auctionDTOStartingBid = auction.getStartingPrice();
		this.auctionDTOEndOn = auction.getEndOn();
	
	}
	
	

}
