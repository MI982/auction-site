package auctionhouse.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ValidationResult {
	
	private String fieldName;
	private String message;
	private Boolean isItValid;
	
	public ValidationResult(){}
	
	public ValidationResult(String fieldName, String message, Boolean isItValid) {
		this.fieldName = fieldName;
		this.message = message;
		this.isItValid = isItValid;
	}

}
