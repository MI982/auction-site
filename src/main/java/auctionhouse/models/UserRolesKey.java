package auctionhouse.models;

import java.io.Serializable;

import lombok.EqualsAndHashCode;

@EqualsAndHashCode
public class UserRolesKey implements Serializable {
	
	private User user;
	private Role role;

}
