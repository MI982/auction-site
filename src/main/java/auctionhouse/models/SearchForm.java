package auctionhouse.models;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchForm {
	
	private String keyword;
	
	private Integer maxPrice;
	
	private Integer minPrice;
	
	private Integer currentPageNumber;
	
	private Integer resultsPerPage;
	
	private String sortingDirection;
	
	private String sortBy;
	
	private String category;

}
