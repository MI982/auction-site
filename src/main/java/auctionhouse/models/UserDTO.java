package auctionhouse.models;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserDTO implements UserDetails{
	
	private Long userId;
	
	private String username;
	
	private String password;
	
	private Integer isActive;
	
	private List<String> roles;
	
	public UserDTO() {}
	
	public UserDTO(User user) {
		this.userId = user.getId();
		this.username = user.getUsername();
		this.password = user.getPassword();
		this.isActive = user.getIsActive();
		this.roles = user.getUserRoles().stream().map(userRoles -> userRoles.getRole().getRole()).collect(Collectors.toList());
	}

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		
		if(roles == null || roles.isEmpty()) return null;
		
		return getRoles().stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
	}

	@Override
	public boolean isAccountNonExpired() {
		// TODO Auto-generated method stub
		return isEnabled();
	}

	@Override
	public boolean isAccountNonLocked() {
		// TODO Auto-generated method stub
		return isEnabled();
	}

	@Override
	public boolean isCredentialsNonExpired() {
		// TODO Auto-generated method stub
		return isEnabled();
	}

	@Override
	public boolean isEnabled() {
		if(isActive > 0) return true; else return false;
	}

}
