package auctionhouse.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;

import auctionhouse.models.Bid;

public interface BidRepository extends JpaRepository<Bid, Long> {

	Page<Bid> findByUserId(Long userId, Specification<Bid> where, PageRequest of);

}
