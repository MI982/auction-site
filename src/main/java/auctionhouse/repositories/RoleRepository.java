package auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import auctionhouse.models.Role;

public interface RoleRepository extends JpaRepository<Role, Long>{

	Role findByRole(String string);
	
	

}
