package auctionhouse.repositories;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import auctionhouse.models.Auction;

public interface AuctionRepository extends JpaRepository<Auction, Long>, JpaSpecificationExecutor<Auction>{

	//Page<Auction> findByUserId(Long userId);

	Page<Auction> findByUserId(Long id, Specification<Auction> where, PageRequest of);

}
