package auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import auctionhouse.models.Article;

public interface ArticleRepository extends JpaRepository<Article, Long> {

}
