package auctionhouse.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import auctionhouse.models.Images;

public interface ImagesRepository extends JpaRepository<Images, Long> {

}
