package auctionhouse.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import auctionhouse.models.UserRoles;
import auctionhouse.models.UserRolesKey;

public interface UserRolesRepository extends JpaRepository<UserRoles, UserRolesKey> {

	List<UserRoles> findAllByUserId(Integer userId);

	

}
