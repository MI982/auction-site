package auctionhouse.controllers;

import java.io.IOException;
import java.security.Principal;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.multipart.MultipartFile;

import auctionhouse.models.Article;
import auctionhouse.models.Auction;
import auctionhouse.models.AuctionArticleWrapper;
import auctionhouse.models.AuctionDTO;
import auctionhouse.models.Category;
import auctionhouse.models.Images;
import auctionhouse.models.LoginUser;
import auctionhouse.models.RegisterUser;
import auctionhouse.models.Role;
import auctionhouse.models.SearchForm;
import auctionhouse.models.User;
import auctionhouse.models.UserDTO;
import auctionhouse.models.UserRoles;
import auctionhouse.services.ArticleService;
import auctionhouse.services.AuctionService;
import auctionhouse.services.BidService;
import auctionhouse.services.CategoryService;
import auctionhouse.services.ImageService;
import auctionhouse.services.RoleService;
import auctionhouse.services.UserRolesService;
import auctionhouse.services.UserService;

@Controller
public class MVCController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private BidService bidService;
	
	@Autowired
	private AuctionService auctionService;
	
	@Autowired
	private ImageService imageService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private UserRolesService userRolesService;
	
	@Autowired
	private CategoryService categoryService;
	
	@Autowired
	private ArticleService articleService;
	
	@Autowired 
	private BCryptPasswordEncoder passwordEncoder;
	
//	@Bean
//	private AuthenticationManager authenticationManagerBean() throws Exception{
//		return super.authenticationManagerBean();
//	}
	
	
	@RequestMapping("/home")
	public String homePage() {
		return "home";
	}
	
	
	@RequestMapping("/loginpage")
	public String loginPage(Model model) {
		model.addAttribute("userSpaceAction", "login");
		model.addAttribute("loginuser", new LoginUser());
		return "home";
	}
	
	
	@RequestMapping("/registerpage")
	public String registerPage(Model model) {
		model.addAttribute("userSpaceAction", "register");
		model.addAttribute("user", new User());
		return "home";
	}
	
	
	@GetMapping("/userauctions")
	public String getAllAuthenticatedUserAuctions(@RequestParam Integer pageRequested, Model model, Principal principal) {
		
		User user = userService.findByUsername(principal.getName());
		
		Page<Auction> auctions = auctionService.findByUserIdAndPagination(user.getId(), pageRequested);
		
		if(auctions != null && auctions.hasContent()) {
			
			model.addAttribute("numberOfPages", auctions.getTotalPages());
			model.addAttribute("currentPage", auctions.getNumber());
			model.addAttribute("auctionList", auctions.getContent());
		}
		
		return "home";
	}
	
	
	@PostMapping("/registeruser")
	public String registerAndAuthenticateUser(@ModelAttribute("user") @Valid RegisterUser userCandidate, 
												BindingResult result,
												Model model,
												HttpServletRequest request,
												AuthenticationManager authenticationManager) {
		
		if(result.hasErrors()) return "home";
		
		User user = convertToUser(userCandidate);
		
		Role defaultRole = roleService.getDefaultRole();
		
		UserRoles userRole = new UserRoles();
		userRole.setUser(user);
		userRole.setRole(defaultRole);
		
		user.getUserRoles().add(userRole);
		user = userService.register(user);
		
		if(user.getId() != null) {
		
			UsernamePasswordAuthenticationToken userDetailsToken = new UsernamePasswordAuthenticationToken(user.getUsername(), user.getPassword());
			userDetailsToken.setDetails(new WebAuthenticationDetails(request));
			
			
			Authentication authentication = authenticationManager.authenticate(userDetailsToken);
			
			SecurityContextHolder.getContext().setAuthentication(authentication);
			
			model.addAttribute("registration", true);
		} else {
			model.addAttribute("error", "Something went wrong, please try again!");
		}
		
		return "home";
	}
	
	
	private User convertToUser(RegisterUser userCandidate) {
		
		User user = new User();
		
		user.setFirstName(userCandidate.getFirstName());
		user.setLastName(userCandidate.getLastName());
		user.setUsername(userCandidate.getUsername());
		user.setPhoneNumber(userCandidate.getPhoneNumber());
		user.setLocation(userCandidate.getLocation());
		user.setPassword(passwordEncoder.encode(userCandidate.getPassword()));
		
		return user;
	}


	@GetMapping("/newauction")
	public String newAuctionPage(Model model) {
		model.addAttribute("auctionOption", "createAuction");
		model.addAttribute("auctionArticleWraper", new AuctionArticleWrapper());
		return "home";
	}
	
	
	@PostMapping(value = "/startauction", consumes = "multipart/form-data")
	public String addNewAuction(@RequestPart(value = "auction", required = true) @Valid AuctionArticleWrapper auctionArticleWrapper, 
								@RequestPart(value = "files", required = true) MultipartFile[] images,
								Principal principal,
								BindingResult bindingResult,
								Model model) {
		
		Auction auction = new Auction();
		Category category = new Category();
		Article article = new Article();
		User user = userService.findByUsername(principal.getName());
		
		category = categoryService.getByName(auctionArticleWrapper.getCategoryName());
		if(category == null) category = categoryService.createNewCategory(auctionArticleWrapper.getCategoryName());
		
		article.setName(auctionArticleWrapper.getArticleName());
		article.setDescription(auctionArticleWrapper.getArticleDescription());
		article = articleService.save(article);
		
		
		if(images != null && images.length > 0) {
			
			List<Images> imageList = new ArrayList<Images>();
			
			for(int i = 0; i<images.length; i++) {
				try {
					Images image = new Images();
					image.setName(images[i].getOriginalFilename().toString());
					image.setPath(imageService.saveToFile(images[i]));
					image.setArticle(article);
					
					imageList.add(image);
					
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			
			imageService.saveAll(imageList);
		}
		
		auction.setCreatedOn(Instant.now().getEpochSecond());
		auction.setEndOn(Instant.parse(LocalDateTime.now().plusDays(auctionArticleWrapper.getAuctionLength()).toString()).getEpochSecond());
		auction.setStartingPrice(auctionArticleWrapper.getAuctionStartingPrice());
		auction.setUser(user);
		auction.setCategory(category);
		auction.setArticle(articleService.findById(article.getId()));
		
		auction = auctionService.save(auction);
		
		model.addAttribute("auctionSpaceAction", "single");
		model.addAttribute("auction", auction);
		
		
		return "home";
	}
	
	
	@GetMapping("/searchauction")
	public String searchAuctionPage(Model model) {
		model.addAttribute("searchform", new Object()); // Object = search wrapper
		model.addAttribute("auctionOption", "searchAuction");
		
		return "home";
	}
	
	
	@GetMapping("/findauction")
	public String findAuction(@ModelAttribute("searchForm") SearchForm searchForm, Model model) { // Object = search wrapper
		
		model.addAttribute("auctionOption", "searchAuction");
		model.addAttribute("searchForm", searchForm);
		
		Page<AuctionDTO> searchResult = auctionService.searchAuctions(searchForm);
		
		if( searchResult != null && searchResult.hasContent()) {
			model.addAttribute("currentPage", searchResult.getNumber());
			model.addAttribute("numberOfPages", searchResult.getTotalPages());
			model.addAttribute("auctionList", searchResult.getContent());
		} else {
			model.addAttribute("error", "Something went wrong!");
		}
		return "home";
	}
	
	
	@GetMapping("/placebid")
	public String placeBid(@RequestParam Integer bidValue, @RequestParam Long auctionId, Model model, Principal principal) {
		
		model.addAttribute("auctionSpaceAction", "singleAuction");
		
		String username = principal.getName();
		AuctionDTO auction = null;
		
		if(bidService.placeBid(auctionId, username, bidValue)) {
			auction = auctionService.findById(auctionId);
			model.addAttribute("singleAuction", auction);
		} else {
			model.addAttribute("singleAuction", "error");
		}
		
		
//		Bid bid = new Bid();
//		User user = userService.findByUsername(principal.getName());
//		Auction auction = auctionService.findById(auctionId);
//		
//		if(auction == null) {
//			model.addAttribute("error", "Auction not found!");
//			return "home";
//		}
//		
//		bid.setBidValue(bidValue);
//		bid.setUser(user);
//		bid.setAuction(auction);
//		
//		int i = 3;
//		
//		do{
//			Long currentTime = null;
//			if((currentTime = Instant.now().getEpochSecond()) < auction.getEndOn()) {
//				bid.setBidTime(currentTime);
//				bid = bidService.save(bid);
//			}
//			
//			i--;
//		} while( bidService.find(bid) == null && i > 0 && Instant.now().getEpochSecond() < auction.getEndOn());
//		
//		if(i<=0) model.addAttribute("error", "Placing bid failed!");
//		if(bid.getBidTime() >= auction.getEndOn()) model.addAttribute("error", "Auction has expired!");
//		
//		model.addAttribute("auctionSpaceAction", "singleAuction");
//		model.addAttribute("singleAuction", new AuctionDTO(auction));
//		
		return "home";
	}
	
	
	@GetMapping("/fakeuser")
	public String fakeUser(Model model) {
		User fakeUser = new User();
		fakeUser.setUsername("Fake User");

		model.addAttribute("user", fakeUser);
		model.addAttribute("userSpaceAction", "loggedin");
		System.out.println(model.containsAttribute("userSpaceAction"));

		return "home";
	}
	
	
	private Page<AuctionDTO> getAuctionsWithPaging(Integer pageNumber, Integer pageSize, Long pageTotal){
		return auctionService.getWithPaging(pageNumber, pageSize, pageTotal);
	}
	
	@GetMapping("/test")
	public UserDTO testMethod(String testString) {
		return userService.getUserForSecurityCheck(testString);
	}
	

	
	

}
