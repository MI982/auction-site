package auctionhouse.controllers;

import java.io.IOException;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import auctionhouse.models.Auction;
import auctionhouse.models.Bid;
import auctionhouse.models.LoginUser;
import auctionhouse.models.RegisterUser;
import auctionhouse.models.User;
import auctionhouse.models.UserDTO;
import auctionhouse.models.UserRoles;
import auctionhouse.responseerrormodels.ErrorResponse;
import auctionhouse.services.AuctionService;
import auctionhouse.services.BidService;
import auctionhouse.services.ImageService;
import auctionhouse.services.JwtService;
import auctionhouse.services.RoleService;
import auctionhouse.services.UserService;

@RestController
@RequestMapping(value = "/r")
public class RestControl {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private BidService bidService;
	
	@Autowired
	private AuctionService auctionService;
	
	@Autowired
	private ImageService imageService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	@Autowired
	private JwtService tokenService;
	
	@Autowired
	private AuthenticationManager authManager;
	
	
	private String dateTimePattern = "HH:mm:ss dd-MM-uuuu";
	
	
	
	@GetMapping(value = "/allusers", produces = "application/json")
	public List<UserDTO> getAllUsers(){
		return userService.getAllUsers().stream().map(UserDTO::new).collect(Collectors.toList());
	}
	
	@GetMapping(value = "/allbids", produces = "application/json")
	public List<Bid> getAllBids(){
		return bidService.getAllBids();
	}
	
	@GetMapping(value = "/allauctions", produces = "application/json")
	public List<Auction> getAllAuctions(){
		return auctionService.getAll();
	}
	
	@GetMapping("/returntoken")
	public ResponseEntity<?> returnToken(Authentication authentication){
		
		HashMap<String, String> payload = new HashMap<>();
		payload.put("jwt", tokenService.createJwToken((UserDTO)authentication.getPrincipal()));
		
		return ResponseEntity.ok(payload);
	}
	
	@PostMapping(value = "/registeruser")
	public ResponseEntity<?> registerUser(@RequestBody @Valid RegisterUser userCandidate, BindingResult result) throws MethodArgumentNotValidException{
		
		if(result.hasErrors()) {
			throw new MethodArgumentNotValidException(null, result);
		}
		
		User user = new User();
		UserRoles userRoles = new UserRoles();
		userRoles.setUser(user);
		userRoles.setRole(roleService.getDefaultRole());
		user.setFirstName(userCandidate.getFirstName());
		user.setLastName(userCandidate.getLastName());
		user.setUsername(userCandidate.getUsername());
		user.setPassword(passwordEncoder.encode(userCandidate.getPassword()));
		user.setLocation(userCandidate.getLocation());
		user.setPhoneNumber(userCandidate.getPhoneNumber());
		user.setIsActive(1);
		user.setUserRoles(Arrays.asList(userRoles));
		
		User savedUser = userService.register(user);
		Long savedUserId = savedUser.getId();
		
		if(savedUserId != null && savedUserId > 0) {
			
			Authentication authentication = authManager.authenticate(
					new UsernamePasswordAuthenticationToken(userCandidate.getUsername(), userCandidate.getPassword())
					);
			
			//UserDTO userDetails = new UserDTO(user);
			UserDTO userDetails = (UserDTO) authentication.getPrincipal();
			
			HashMap<String, String> response = new HashMap<>();
			response.put("jwt", tokenService.createJwToken(userDetails));
			
			return ResponseEntity.ok(response);
		}
		return null;
	}
	
//	@PostMapping(value = "/addauction", produces = "application/json")
//	public Auction addAuction(@RequestBody(required = true) @Valid Auction auction, @RequestPart(value = "file", required = true) MultipartFile[] images) {
//		
//		
//		
//		return auction;
//	}
	
	@PostMapping("/login")
	public ResponseEntity<?> loginAndCreateJwt(@RequestBody @Valid LoginUser loginUser) throws MethodArgumentNotValidException {
		User user = userService.findByUsername(loginUser.getUsername());
		
		HashMap<String, String> response = new HashMap<>();
		
		
		if(user == null) {
			response.put("error", "Username not found!");
			return ResponseEntity.status(403).body(response);
		}
		
		if(user.getIsActive()>0) {
			response.put("error", user.getUsername()+" is already logged in!");
			return ResponseEntity.badRequest().body(response);
		}
		
		if(passwordEncoder.matches(loginUser.getPassword(), user.getPassword())) {
			
			user.setIsActive(1);
		
			response.put("jwt", tokenService.createJwToken(new UserDTO(user)));
			
			return ResponseEntity.ok(response);
		} else {
			response.put("error", "Wrong password!");
			return ResponseEntity.badRequest().body(response);
		}
	}
	
	@GetMapping("/logout")
	public ResponseEntity<?> logoutTest() {
		
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		
		User user = userService.findByUsername(username);
		if(user == null) return ResponseEntity.status(HttpStatus.NOT_FOUND).body("User not found!");
		user.setIsActive(0);
		user = userService.register(user);
		
		if(user.getIsActive() == 0) return ResponseEntity.ok(null); 
		else return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("User is already logged out!");
		
		
//		new SecurityContextLogoutHandler().logout(request, null, null);
//		
//		try {
//			response.sendRedirect(request.getHeader("referer"));
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
	}
	
	@GetMapping("/myusername")
	public ResponseEntity<?> usernameTest() {
		return ResponseEntity.ok(((UserDTO)SecurityContextHolder.getContext().getAuthentication().getPrincipal()).getUsername());
	}
	
	@PostMapping("/addpic")
	public ResponseEntity<String> addPicture(@RequestParam("image") MultipartFile image){
		
		try {
			String savedImagePath = imageService.saveToFile(image);
			
			Long imageId = imageService.saveToDb(image.getOriginalFilename().toString(), savedImagePath);
			
			return new ResponseEntity<String>("Id: "+imageId,HttpStatus.OK);
			
		} catch (IOException e) {
			e.printStackTrace();
			ErrorResponse.createNew()
							.addStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
							.addMessage("Something went wrong!")
							.addTimestamp(LocalDateTime.now().format(DateTimeFormatter.ofPattern(dateTimePattern)));
			return new ResponseEntity<>(ErrorResponse.createNew()
										.addStatus(HttpStatus.INTERNAL_SERVER_ERROR.value())
										.addMessage("Something went wrong!")
										.addTimestamp(LocalDateTime.now().format(DateTimeFormatter.ofPattern(dateTimePattern)))
										.toString()
					, HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
	}
	
	@GetMapping(value = "/getpic", produces = MediaType.IMAGE_JPEG_VALUE)
	public byte[] getPicture(@RequestParam Long imageId){
		
		try {
			return imageService.getFromFile(imageId);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
	
	@GetMapping("/rtest")
	public UserDTO testMethod(@RequestParam("testString")String testString) {
		return userService.getUserForSecurityCheck(testString);
	}
	
	@PostMapping("/errortest")
	public ResponseEntity<?> testBadCredentialsError(@RequestBody @Valid RegisterUser registerUser) {
		return ResponseEntity.ok(registerUser);
	}

}
