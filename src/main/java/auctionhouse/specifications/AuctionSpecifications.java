package auctionhouse.specifications;

import java.time.Instant;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.data.jpa.domain.Specification;

import auctionhouse.models.Article_;
import auctionhouse.models.Auction;
import auctionhouse.models.Auction_;
import auctionhouse.models.Category_;
import auctionhouse.models.User_;

public final class AuctionSpecifications {
	
	private AuctionSpecifications() {}
	
	@SuppressWarnings("serial")
	public static Specification<Auction> nameLike(String keyword){
		
//		String searchKeyword = prepare(keyword);
		
		return new Specification<Auction>() {

			@Override
			public Predicate toPredicate(Root<Auction> root, CriteriaQuery<?> query, CriteriaBuilder criteriaBuilder) {
				return criteriaBuilder.like(root.join(Auction_.article).get(Article_.name), keyword);
			}
			
		};
	}
	
	public static Specification<Auction> descriptionLike(String keyword){
		
//		String searchKeyword = prepare(keyword);
		
		return (root, query, criteriaBuilder) -> {
			return criteriaBuilder.like(root.join(Auction_.article).get(Article_.description), keyword);
		};
	}
	
	public static Specification<Auction> categoryLike(String keyword){
		
//		String searchKeyword = prepare(keyword);
		
		return (root, query, criteriaBuilder) -> {
			return criteriaBuilder.like(root.join(Auction_.category).get(Category_.name), keyword);
		};
	}
	
	public static Specification<Auction> priceGreaterThan(Integer minPrice){
		
		return (root, query, criteriaBuilder) -> {
			return criteriaBuilder.greaterThanOrEqualTo(root.get(Auction_.startingPrice), minPrice);
		};
	}
	
	public static Specification<Auction> priceLessThan(Integer maxPrice){
		
		return (root, query, criteriaBuilder) -> {
			return criteriaBuilder.lessThanOrEqualTo(root.get(Auction_.startingPrice), maxPrice);
		};
	}
	
	public static Specification<Auction> hasNotExpired(){
		
		return (root, query, criteriaBuilder) -> { 
			return criteriaBuilder.gt(root.get(Auction_.endOn), Instant.now().getEpochSecond());
		};
	}
	
	public static Specification<Auction> userIdIs(Long userId){
		return (root, query, criteriaBuilder) -> {
			return criteriaBuilder.equal(root.join(Auction_.user).get(User_.id), userId);
		};
	}
	
	public static Specification<Auction> usernameIs(String username){
		return (root, query, criteriaBuilder) -> {
			return criteriaBuilder.equal(root.join(Auction_.user).get(User_.username), username);
		};
	}
	
	
//	public static Specification<Auction> userId(Integer userId){
//		
//		return (root, query, criteriaBuilder) -> {
//			return criteriaBuilder.
//		};
//	}
	
	
//	private static String prepare(String s) {
//		
//		if(Strings.isBlank(s)) return "%";
//		
//		StringBuilder keyword = new StringBuilder();
//		keyword.append("%");
//		keyword.append(s.toLowerCase());
//		keyword.append("%");
//		
//		return keyword.toString();
//	}

}
