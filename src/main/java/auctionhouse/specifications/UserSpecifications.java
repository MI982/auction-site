package auctionhouse.specifications;

import javax.persistence.criteria.Join;

import org.springframework.data.jpa.domain.Specification;

import auctionhouse.models.User;
import auctionhouse.models.UserRoles_;
import auctionhouse.models.User_;

public final class UserSpecifications {
	
	private UserSpecifications() {}
	
	
	@SuppressWarnings("unchecked")
	public static Specification<User> usernameEquals(String username){
		
		return (root, query, criteriaBuilder) -> {
			( (Join<Object, Object>) root.fetch(User_.USER_ROLES)).fetch(UserRoles_.ROLE);
			return criteriaBuilder.equal(root.get(User_.USERNAME), username);
		};
	}

}
