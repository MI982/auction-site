package auctionhouse.specifications;

import java.time.Instant;

import org.springframework.data.jpa.domain.Specification;

import auctionhouse.models.Auction_;
import auctionhouse.models.Bid;
import auctionhouse.models.Bid_;

public final class BidSpecifications {
	
	private BidSpecifications(){}
	
	
	public static Specification<Bid> hasNotExpired(){
		
		return (root, query, criteriaBuilder) -> {
			return criteriaBuilder.gt(root.join(Bid_.AUCTION).get(Auction_.END_ON), Instant.now().getEpochSecond());
		};
	}
	
	public static Specification<Bid> highestBid(){
		return (root, query, criteriaBuilder) -> {
			return criteriaBuilder.equal(root.get(Bid_.bidValue), criteriaBuilder.max(root.get(Bid_.bidValue)));
		};
	}

}
