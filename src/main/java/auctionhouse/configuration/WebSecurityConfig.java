package auctionhouse.configuration;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import auctionhouse.customexceptions.CustomAuthenticationFailureHandler;
import auctionhouse.customexceptions.CustomAuthenticationSuccessHandler;
import auctionhouse.customexceptions.CustomRestAccessDeniedHandler;
import auctionhouse.customexceptions.CustomRestAuthenticationEntryPoint;
import auctionhouse.filters.CustomJwtLogoutHandler;
import auctionhouse.filters.FilterExceptionResolver;
import auctionhouse.filters.JwtAuthenticationFilter;
import auctionhouse.filters.JwtAuthorizationFilter;


@EnableWebSecurity
public class WebSecurityConfig {
	
	@Autowired
	private DataSource dataSource;
	
	@Autowired
	private UserDetailsService userDetailsService;
	
	
	
	@Bean
	public BCryptPasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	
	
//	@Override
//	@Bean
//	public AuthenticationManager authenticationManagerBean() throws Exception {
//		return super.authenticationManagerBean();
//	}
	
	@Configuration
	@Order(1)
	public class RestSecurity extends WebSecurityConfigurerAdapter {
		
		@Bean
		@Override
		public AuthenticationManager authenticationManagerBean() throws Exception {
			return super.authenticationManagerBean();
		}
		
		@Autowired
		private CustomRestAccessDeniedHandler accessDeniedHandler;
		
//		@Autowired
//		private CustomRestAuthenticationEntryPoint authenticationEntryPoint;
		
		@Autowired
		private CustomAuthenticationFailureHandler failureHandler;
		@Autowired
		private CustomAuthenticationSuccessHandler successHandler;
		
		@Bean
		public JwtAuthenticationFilter jwtAuthenticationFilter() throws Exception {
			JwtAuthenticationFilter filter = new JwtAuthenticationFilter();
			filter.setAuthenticationManager(authenticationManagerBean());
			filter.setAuthenticationFailureHandler(failureHandler);
			filter.setAuthenticationSuccessHandler(successHandler);
			filter.setRequiresAuthenticationRequestMatcher(new AntPathRequestMatcher("/login", "POST"));
			return filter;
		}
		
		@Autowired
		public JwtAuthorizationFilter jwtAuthorizationFilter;
		
		@Autowired
		public FilterExceptionResolver filterExceptionResolver;
		
		@Autowired
		public CustomJwtLogoutHandler logoutHandler;
		
		
		@Override
		protected void configure(AuthenticationManagerBuilder auth) throws Exception {
			
			auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder());
		}
		
		private String[] allowedEndpoints = {"/r/registeruser", "/r/login", "/login", "/r/errortest"};
		
		@Override
		protected void configure(HttpSecurity hSec) throws Exception {
			
			hSec.csrf().disable()
				.authorizeRequests()
				.antMatchers(allowedEndpoints).permitAll()
				.anyRequest().authenticated()
				.and()
				.addFilterAt(jwtAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class)
				.addFilterBefore(jwtAuthorizationFilter, JwtAuthenticationFilter.class)
				.addFilterBefore(filterExceptionResolver, JwtAuthorizationFilter.class)
				.exceptionHandling().accessDeniedHandler(accessDeniedHandler)
				.and()
				.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS)
				.and()
				.logout().addLogoutHandler(logoutHandler);
			
//			hSec.addFilterBefore(jwtAuthenticationFilter, UsernamePasswordAuthenticationFilter.class);
//			hSec.addFilterBefore(filterExceptionResolver, JwtAuthenticationFilter.class);
				
			
		}
		
	}
	
	
	@Configuration
	@Order(2)
	public class WebSecurity extends WebSecurityConfigurerAdapter {
		
		
		@Autowired
		protected void configAuthentication(AuthenticationManagerBuilder amb) throws Exception {
			
			amb.jdbcAuthentication()
				.dataSource(dataSource)
				.passwordEncoder(passwordEncoder())
				.usersByUsernameQuery("select username, password, is_active from user where username=?")
				.authoritiesByUsernameQuery("select username, role.name as role from user join user_roles on user.user_id = user_user_id join role on role_id = user_roles.roles_role_id where username = ?");
		}
		
		@Override
		protected void configure(HttpSecurity hSec) throws Exception{
			
			hSec.authorizeRequests()
					.antMatchers("/login", "/home", "/loginpage", "/registerpage", "/registeruser", "/test").permitAll()
					.anyRequest().authenticated()
				.and()
				.formLogin()
					.loginPage("/login").permitAll()
					.usernameParameter("username")
					.passwordParameter("password")
					.defaultSuccessUrl("/home")
				.and()
				.logout()
//					.invalidateHttpSession(true)
//					.clearAuthentication(true)
				.and()
				.csrf();
			
		}
		
	}
	

}
