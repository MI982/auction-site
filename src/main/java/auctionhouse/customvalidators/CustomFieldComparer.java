package auctionhouse.customvalidators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.BeanWrapperImpl;

public class CustomFieldComparer implements ConstraintValidator<FieldComparer, Object>{
	
	private String comparedTo;
	private String toBeCompared;
	
	@Override
	public void initialize(FieldComparer fieldComparer) {
		
		this.comparedTo = fieldComparer.comparedTo();
		this.toBeCompared = fieldComparer.toBeCompared();
	}
	

	@Override
	public boolean isValid(Object object, ConstraintValidatorContext constraintValidatorContext) {
		
//		if (object == null || comparedTo.isEmpty() || toBeCompared.isEmpty() || (comparedTo == null && toBeCompared == null)) {
//			return false;
//		} else {
//			
//			String firstFieldGetterName = getMethodNameFor(comparedTo);
//			String secondFieldGetterName = getMethodNameFor(toBeCompared);
//			
//			Method[] methods = object.getClass().getMethods();
//			
//
//			Method firstFieldGetter = null;
//			Method secondFieldGetter = null;
//			
//			if(methods != null && methods.length>1) {
//				
//				for(Method method : Arrays.asList(methods)) {
//					if(method.getName().equals(firstFieldGetterName)) firstFieldGetter = method;
//					if(method.getName().contentEquals(secondFieldGetterName)) secondFieldGetter = method;
//				}
//				
//			}
//			
//			if(firstFieldGetter != null && secondFieldGetter != null) {
//				
//				try {
//					return firstFieldGetter.invoke(object).equals(secondFieldGetter.invoke(object));
//				} catch (IllegalAccessException | IllegalArgumentException | InvocationTargetException e) {
//					return false;
//				}
//				
//			}
//			
//		}
		
		
//		try {
//			Object first= PropertyUtils.getProperty(object, comparedTo);
//			Object second = PropertyUtils.getProperty(object, toBeCompared);
//			
//			if(first != null && second != null) return first.equals(second);
//			
//		} catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException e) {
//			return false;
//		}
		
		if( object != null) {
			BeanWrapperImpl wrapper = new BeanWrapperImpl(object);
		
			if(wrapper.isReadableProperty(comparedTo) && wrapper.isReadableProperty(toBeCompared)) {
				
				return wrapper.getPropertyValue(comparedTo).equals(wrapper.getPropertyValue(toBeCompared));
				
			}
		}
		
		
		return false;
	}
	
//	private String getMethodNameFor(String fieldName) {
//		
//		return "get"+fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
//	}

}
