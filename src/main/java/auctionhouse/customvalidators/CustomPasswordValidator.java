package auctionhouse.customvalidators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.apache.logging.log4j.util.Strings;

public class CustomPasswordValidator implements ConstraintValidator<ValidatePassword, String> {
	
	
	@Override
	public void initialize(ValidatePassword validatePassword) {
		ConstraintValidator.super.initialize(validatePassword);
	}
	
	@Override
	public boolean isValid(String password, ConstraintValidatorContext constraintValidatorContext) {
		
		StringBuilder message = new StringBuilder();
		boolean response = true;
		
		if (Strings.isBlank(password)) {
			message.append("Password must be provided!");
			response = false;
		}
		
		if(response && password.contains(" ")){
			message.append("Password must not contain empty spaces!");
			response = false;
		}
		
		if(response && !password.matches("[A-Z]")) {
			message.append("Password must contain at leas one capital letter!");
			response = false;
		}
		
		if(response && !password.matches("[0-9]")) {
			message.append("Password must contain at leas one number!");
			response = false;
		}
		
		if(!response) {
			constraintValidatorContext.buildConstraintViolationWithTemplate(message.toString())
									  .addConstraintViolation()
								      .disableDefaultConstraintViolation();
		}
		
		return response;
	}

}
