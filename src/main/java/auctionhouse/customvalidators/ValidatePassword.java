package auctionhouse.customvalidators;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Retention(RetentionPolicy.RUNTIME)
@Target(value = { ElementType.FIELD, ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Constraint(validatedBy = CustomPasswordValidator.class)
public @interface ValidatePassword {
	
	String message() default "Default Password Error";
	
	Class<?>[] groups() default {};
	
	Class<? extends Payload>[] payload() default {};

}
