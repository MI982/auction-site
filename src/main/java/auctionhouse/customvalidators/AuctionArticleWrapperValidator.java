package auctionhouse.customvalidators;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import auctionhouse.models.AuctionArticleWrapper;
import auctionhouse.models.ValidationResult;
import auctionhouse.services.CategoryService;
import lombok.Getter;
import lombok.Setter;

@Component
public class AuctionArticleWrapperValidator implements CustomValidator{
	
	private AuctionArticleWrapper aaw;
	
	@Autowired
	private CategoryService categoryService;
	
	@Getter
	@Setter
	private String message;

	@Override
	public List<ValidationResult> validate(Object object) {
		
		if(AuctionArticleWrapper.class.isInstance(object)) {
			
			aaw = (AuctionArticleWrapper) object;
			
			List<ValidationResult> result = new ArrayList<ValidationResult>();
			
			if(articleNameIsNotValid()) {
				result.add(new ValidationResult("articleName", getMessage(), true));
			}
			
			if(categoryNameIsNotValid()) {
				result.add(new ValidationResult("categoryName", getMessage(), true));
			}
			
			if(auctionLengthIsNotValid()) {
				result.add(new ValidationResult("auctionLength", getMessage(), true));
			}
			
			if(auctionStartingPriceIsNotValid()) {
				result.add(new ValidationResult("auctionStartingPrice", getMessage(), true));
			}
			
			return result;
			
		}
		return null;
	}

	private boolean auctionStartingPriceIsNotValid() {
		
		Integer startingPrice = aaw.getAuctionStartingPrice();
		
		if(startingPrice == null) {
			setMessage("Starting price for the auction must be provided!");
			return true;
		}
		
		if(startingPrice < 0) {
			setMessage("Starting price you provided is a negative number. WTF!?!");
			return true;
		}
		return false;
	}

	private boolean auctionLengthIsNotValid() {
		
		Integer auctionLength = aaw.getAuctionLength();
		if(auctionLength == null) {
			setMessage("Auction length must be provided!");
			return true;
		}
		
		if(auctionLength < 1) {
			setMessage("Auction must last at least 1 day!");
			return true;
		}
		
		if(auctionLength > 30) {
			setMessage("Auction can only be up for 30 days or less!");
			return true;
		}
		return false;
	}

	private boolean categoryNameIsNotValid() {
		
		boolean result = Strings.isBlank(aaw.getCategoryName());
		if(result) {
			setMessage("Category name must be provided!");
			return result;
		}
		
		result = categoryService.getByName(aaw.getCategoryName()) == null;
		if(result) setMessage("Category does not exist!");
		return result;
	}

	private boolean articleNameIsNotValid() {
		
		boolean result = Strings.isBlank(aaw.getArticleName());
		if(result) setMessage("Article name must be provided!");
		return result;
	}

}
