package auctionhouse.customvalidators;

import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import auctionhouse.models.ValidationResult;
import lombok.Getter;
import lombok.Setter;



public class CustomValidateBy implements ConstraintValidator<ValidateBy, Object>{
	
	@Autowired
	private ApplicationContext appContext;
	
	private CustomValidator validator;
	
	@Override
	public void initialize(ValidateBy constraintAnnotation) {
		
		//validator = constraintAnnotation.customValidator().newInstance();
		
		validator = appContext.getBean(constraintAnnotation.customValidator());
	
	}

	@Override
	public boolean isValid(Object object, ConstraintValidatorContext context) {
		
		if(validator == null) throw new RuntimeException("Validator is null!");
		
		if(object == null) throw new RuntimeException("Validation target is null!");
		
		List<ValidationResult> resultList = validator.validate(object);
		
		boolean result = resultList == null || resultList.isEmpty();
		
		if(!result) {
			context.disableDefaultConstraintViolation();
			
			for(ValidationResult validationResult : resultList) {
				context.buildConstraintViolationWithTemplate(validationResult.getMessage())
						.addPropertyNode(validationResult.getFieldName())
						.addConstraintViolation();
			}
		}
		
		return result;
	}
	
//	@Getter
//	@Setter
//	public class ValidationResult{
//		
//		private String fieldName;
//		private String message;
//		private Boolean isItValid;
//		
//		public ValidationResult(){}
//		
//		public ValidationResult(String fieldName, String message, Boolean isItValid) {
//			this.fieldName = fieldName;
//			this.message = message;
//			this.isItValid = isItValid;
//		}
//	}

}
