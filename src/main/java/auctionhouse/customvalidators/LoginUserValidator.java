package auctionhouse.customvalidators;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.util.Strings;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import auctionhouse.models.LoginUser;
import auctionhouse.models.ValidationResult;
import lombok.Getter;
import lombok.Setter;

@Component
public class LoginUserValidator implements CustomValidator, Validator{
	
	private LoginUser loginUser;
	
	@Getter
	@Setter
	private String message;

	@Override
	public List<ValidationResult> validate(Object object) {
		
		if(LoginUser.class.isInstance(object)) {
			
			loginUser = (LoginUser) object;
			
			List<ValidationResult> result = new ArrayList<ValidationResult>();
			
			if(usernameIsNotValid()) {
				result.add(new ValidationResult("username", getMessage(), true));
			}
			
			if(passwordIsNotValid()) {
				result.add(new ValidationResult("password", getMessage(), true));
			}
			
			return result;
		}
		
		
		return null;
	}

	private boolean passwordIsNotValid() {
		boolean result = Strings.isBlank(loginUser.getPassword());
		if(result) setMessage("Password must be provided!");
		return result;
	}

	private boolean usernameIsNotValid() {
		boolean result = Strings.isBlank(loginUser.getUsername());
		if(result) setMessage("Username must be provided!");
		return result;
	}

	@Override
	public boolean supports(Class<?> clazz) {
		return LoginUser.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		List<ValidationResult> result = validate(target);
		for(ValidationResult validationResult : result) {
			errors.rejectValue(validationResult.getFieldName(), null, validationResult.getMessage());
		}
		
		
	}

}
