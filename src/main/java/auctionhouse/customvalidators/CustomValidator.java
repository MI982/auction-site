package auctionhouse.customvalidators;

import java.util.List;

import auctionhouse.models.ValidationResult;

public interface CustomValidator {
	
	public List<ValidationResult> validate(Object object);

}
