package auctionhouse.customvalidators;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import auctionhouse.models.RegisterUser;

public class CustomPasswordMatcher implements ConstraintValidator<PasswordMatcher, RegisterUser> {

	
	@Override
	public boolean isValid(RegisterUser registerUser, ConstraintValidatorContext context) {
		
		return registerUser.getPassword().contentEquals(registerUser.getRepeatedPassword());
	}

}
