package auctionhouse.customvalidators;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.BeanWrapperImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Configurable;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import auctionhouse.models.RegisterUser;
import auctionhouse.models.ValidationResult;
import auctionhouse.repositories.UserRepository;
import auctionhouse.services.UserService;
import lombok.Getter;
import lombok.Setter;

@Component
public class RegisterUserValidator implements CustomValidator{
	
	@Autowired
	private UserRepository userRepository;
	
	private RegisterUser registerUser;
	
	@Getter
	@Setter
	private String message;
	

	@Override
	public List<ValidationResult> validate(Object object) {
		
		
		if(RegisterUser.class.isInstance(object)) {
			
			registerUser = (RegisterUser) object;
			
			List<ValidationResult> result = new ArrayList<ValidationResult>();
			
			if(firstNameIsNotValid()) {
				result.add(new ValidationResult("firstName", "First name must be provided!", true));
			}
			
			if(lastNameIsNotValid()) {
				result.add(new ValidationResult("lastName", "Last name must be provided!", true));
			}
			
			if(usernameIsNotValid()) {
				result.add(new ValidationResult("username", getMessage(), true));
			}
			
			if(passwordIsNotValid()) {
				result.add(new ValidationResult("password", getMessage(), true));
			}
			
			if(repeatedPasswordIsNotValid()) {
				result.add(new ValidationResult("repeatedPassword", getMessage(), true));
			}
			
			if(locationIsNotValid()) {
				result.add(new ValidationResult("location", "Location must be provided!", true));
			}
			
			if(phoneNumberIsNotValid()) {
				result.add(new ValidationResult("phoneNumber", "Phone number must be provided!", true));
			}
			
			return result;
			
		}
		
		return null;
		
	}
	
	private boolean phoneNumberIsNotValid() {
		return Strings.isBlank(registerUser.getPhoneNumber());
	}

	private boolean locationIsNotValid() {
		return Strings.isBlank(registerUser.getLocation());
	}

	private boolean repeatedPasswordIsNotValid() {
		
		if(getMessage() != null || Strings.isNotBlank(getMessage())) setMessage(null);
		
		boolean result = false;
		
		boolean isNotBlank = Strings.isNotBlank(registerUser.getRepeatedPassword());
		
		if(isNotBlank) {
			
			if(!registerUser.getPassword().equals(registerUser.getRepeatedPassword())) {
				setMessage("Passwords don't match!");
				result = true;
			}
			
		} else {
			setMessage("Password must be confirmed!");
			result = true;
		}
		
		return result;
		
	}

	private boolean passwordIsNotValid() {
		
		if(getMessage() != null || Strings.isNotBlank(getMessage())) setMessage(null);
		
		boolean blank = Strings.isBlank(registerUser.getPassword());
		
		
		if(!blank) {
			
			if(!blank && registerUser.getPassword().contains(" ")){
				setMessage("Password must not contain empty spaces!");
				blank = true;
			}
			
			if(!blank && registerUser.getPassword().length() < 6) {
				setMessage("Password must be at least 6 characters long!");
				blank = true;
			}
			
			if(!blank) {
				boolean thereIsAnUpperCase = false;
				for(char c: registerUser.getPassword().toCharArray()) {
					if(Character.isUpperCase(c)) {
						thereIsAnUpperCase = true;
						break;
					};
				}
				if(!thereIsAnUpperCase) {
					setMessage("Password must contain at leas one capital letter!");
					blank = true;
				}
			}
			
			if(!blank && !registerUser.getPassword().matches(".*\\d+.*")) {
				setMessage("Password must contain at leas one number!");
				blank = true;
			}
			
		} else {
			setMessage("Password Must be provided!");
			
		}
		
		return blank;
	}

	private boolean usernameIsNotValid() {
		
		if(getMessage() != null || Strings.isNotBlank(getMessage())) setMessage(null);
		
		boolean result = Strings.isBlank(registerUser.getUsername());
		
		if(result) setMessage("Username must be provided!");
		
		if(!result && registerUser.getUsername().length() < 5) {
			setMessage("Username must have at least 5 characters!");
			result = true;
		}
			
		if(!result) {
			if(userRepository.existsByUsername(registerUser.getUsername())) {
			setMessage("Username already exists!");
			result = true;	
			}
		}
		
		return result;
	}

	private boolean lastNameIsNotValid() {
		
		return Strings.isBlank(registerUser.getLastName());
	}

	
	private boolean firstNameIsNotValid() {
		
		return Strings.isBlank(registerUser.getFirstName());
	}

}
