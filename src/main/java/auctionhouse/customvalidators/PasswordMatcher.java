package auctionhouse.customvalidators;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

@Retention(value = RetentionPolicy.RUNTIME)
@Target(value = { ElementType.TYPE, ElementType.ANNOTATION_TYPE })
@Constraint(validatedBy = CustomPasswordMatcher.class)
public @interface PasswordMatcher {

	String message() default "They ain't matching!";
	
	Class<?>[] groups() default {};
	
	Class<? extends Payload>[] payload() default {};

}
