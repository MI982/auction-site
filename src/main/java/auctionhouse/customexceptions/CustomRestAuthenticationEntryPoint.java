package auctionhouse.customexceptions;

import java.io.IOException;
import java.io.OutputStream;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.fasterxml.jackson.databind.ObjectMapper;

import auctionhouse.responseerrormodels.ErrorResponse;

@Component
public class CustomRestAuthenticationEntryPoint implements AuthenticationEntryPoint{
	
	private static final Logger log = LoggerFactory.getLogger(CustomRestAuthenticationEntryPoint.class);

	private String dateTimePattern = "HH:mm:ss dd-MM-uuuu";

	@Override
	public void commence(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		log.info("CustomRestAuthenticationEntryPoint activated!");
		log.info(exception.getClass()+" caught!");
		log.info(exception.getCause()+" caught!");
		ErrorResponse errorResponse = null;
		String timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern(dateTimePattern));
		if(LoginUserException.class.isInstance(exception.getCause())){
			BindingResult result = ((LoginUserException) exception.getCause()).getBindingResult();
			HashMap<String, HashMap<String, String>> errors = new HashMap<String,HashMap<String,String>>();
			HashMap<String, String> errorMessageHolder = new HashMap<String,String>();
			
			result.getAllErrors().forEach((error)-> {
				String fieldName = ((FieldError) error).getField();
				String errorMessage = error.getDefaultMessage();
				errorMessageHolder.put(fieldName, errorMessage);
			});
			
			errors.put("error", errorMessageHolder);
			errorResponse = new ErrorResponse(HttpStatus.BAD_REQUEST.value(), errors.toString(), timestamp);
		}else {
			errorResponse = new ErrorResponse(HttpStatus.UNAUTHORIZED.value(), exception.getMessage(), timestamp);
		}
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("application/json");
		//response.setStatus(HttpStatus.UNAUTHORIZED.value());
		OutputStream out = response.getOutputStream();
		mapper.writeValue(out, errorResponse);
		out.flush();
		out.close();
		
	}

}
