package auctionhouse.customexceptions;

import org.springframework.validation.BindingResult;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginUserException extends Exception{
	
	BindingResult bindingResult;
	
	public LoginUserException() {}
	
	public LoginUserException(BindingResult bindingResult) {
		this.bindingResult = bindingResult;
	}

}
