package auctionhouse.customexceptions;

import java.io.IOException;
import java.io.OutputStream;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import auctionhouse.responseerrormodels.ErrorResponse;

@Component
public class CustomRestAccessDeniedHandler implements AccessDeniedHandler{
	
	
	private static final Logger log = LoggerFactory.getLogger(CustomRestAccessDeniedHandler.class);

	
	private String dateTimePattern = "HH:mm:ss dd-MM-uuuu";

	@Override
	public void handle(HttpServletRequest request, HttpServletResponse response,
			AccessDeniedException exception) throws IOException, ServletException {
		
		String timestamp = LocalDateTime.now().format(DateTimeFormatter.ofPattern(dateTimePattern));
		
		log.info("Access denied at "+timestamp);
		
		log.info(exception.getCause()+" cought!");
		log.info(exception.getClass()+" cought!");
		
		ErrorResponse error = new ErrorResponse(HttpStatus.FORBIDDEN.value(), exception.getMessage(), timestamp);
		
		ObjectMapper mapper = new ObjectMapper();
		response.setContentType("application/json");
		response.setStatus(HttpStatus.FORBIDDEN.value());
		OutputStream out = response.getOutputStream();
		mapper.writeValue(out, error);
		out.flush();
		out.close();
		
	}

}
