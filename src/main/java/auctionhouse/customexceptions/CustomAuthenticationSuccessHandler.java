package auctionhouse.customexceptions;

import java.io.IOException;
import java.io.OutputStream;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

import auctionhouse.models.UserDTO;
import auctionhouse.services.JwtService;

@Component
public class CustomAuthenticationSuccessHandler implements AuthenticationSuccessHandler{
	
	@Autowired
	private JwtService tokenService;

	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		
		UserDTO userDetails = (UserDTO) authentication.getPrincipal();
		
		response.setStatus(HttpStatus.OK.value());
		ObjectMapper mapper = new ObjectMapper();
		HashMap<String, String> successfulResponse = new HashMap<>();
		successfulResponse.put("jwt", tokenService.createJwToken(userDetails));
		
		//response.addHeader(AUTHORIZATION, BEARER+tokenService.createJwToken(userDetails));
		OutputStream out = response.getOutputStream();
		
		mapper.writeValue(out, successfulResponse);
		
		out.flush();
		out.close();
		
	}

}
