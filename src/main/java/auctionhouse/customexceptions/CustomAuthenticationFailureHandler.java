package auctionhouse.customexceptions;

import java.io.IOException;
import java.io.OutputStream;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.stereotype.Component;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import com.fasterxml.jackson.databind.ObjectMapper;

import auctionhouse.controlleradvice.GlobalExceptionHandler;
import auctionhouse.responseerrormodels.ErrorResponse;
import auctionhouse.responseerrormodels.LoginUserErrorResponse;

@Component
public class CustomAuthenticationFailureHandler implements AuthenticationFailureHandler{
	
	
	private static final Logger log = LoggerFactory.getLogger(CustomAuthenticationFailureHandler.class);

	
	@Autowired
	private GlobalExceptionHandler exceptionHandler;

	@Override
	public void onAuthenticationFailure(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException exception) throws IOException, ServletException {
		
		log.info("Failure handler cought "+exception.getClass());
		
		exceptionHandler.handle(exception, response);
		
	}

}
