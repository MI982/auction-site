package auctionhouse.filters;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.logout.LogoutHandler;
import org.springframework.stereotype.Component;

import auctionhouse.models.User;
import auctionhouse.models.UserDTO;
import auctionhouse.services.UserService;

@Component
public class CustomJwtLogoutHandler implements LogoutHandler{
	
	@Autowired
	private UserService userService;

	@Override
	public void logout(HttpServletRequest request, HttpServletResponse response, Authentication authentication) {
		UserDTO activeUser = (UserDTO) authentication.getPrincipal();
		User user = userService.findByUsername(activeUser.getUsername());
		user.setIsActive(0);
		userService.register(user);
	}

}
