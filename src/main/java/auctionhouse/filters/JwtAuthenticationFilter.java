package auctionhouse.filters;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.MalformedParametersException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.core.MethodParameter;
import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.server.authentication.RedirectServerAuthenticationSuccessHandler;
import org.springframework.validation.BindingResult;
import org.springframework.validation.DataBinder;
import org.springframework.web.bind.MethodArgumentNotValidException;

import com.fasterxml.jackson.databind.ObjectMapper;

import auctionhouse.controlleradvice.GlobalExceptionHandler;
import auctionhouse.customexceptions.LoginUserException;
import auctionhouse.customvalidators.CustomValidateBy;
import auctionhouse.customvalidators.LoginUserValidator;
import auctionhouse.models.LoginUser;
import auctionhouse.models.UserDTO;
import auctionhouse.models.ValidationResult;
import auctionhouse.services.JwtService;
import auctionhouse.services.UserService;

public class JwtAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
	
	
	private static final Logger log = LoggerFactory.getLogger(JwtAuthenticationFilter.class);
	
	@Autowired
	private LoginUserValidator validator;
	
//	private static final String AUTHORIZATION = "Authorization";
//	
//	private static final String BEARER = "Bearer ";
	
	@Override
	public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
			throws AuthenticationException {
		
		log.info("Attempting authentication!");
		
		LoginUser loginUser = getLoginUserFrom(request, response);
		
		log.info(loginUser.toString());
		
		DataBinder binder = new DataBinder(loginUser);
		
		binder.setValidator(validator);
		binder.validate();
		
		BindingResult bindingResult = binder.getBindingResult();
		
		
		if(!bindingResult.hasErrors()) {
			
			UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(loginUser.getUsername(),
																									loginUser.getPassword());
			
			return getAuthenticationManager().authenticate(authToken);
			
		} else {
			log.info("LoginUser has errors!");
			throw new BadCredentialsException("Bad credentials!", new LoginUserException(bindingResult));
		}
	}
	
	
	@Override
	protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
			Authentication authResult) throws IOException, ServletException {
		log.info("User is authenticated: "+authResult.isAuthenticated());
		log.info("Authorities: "+authResult.getAuthorities());
		getSuccessHandler().onAuthenticationSuccess(request, response, authResult);
	}
	
	

	@Override
	protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
			AuthenticationException failed) throws IOException, ServletException {
		log.info(failed.getClass()+" cought!");
		log.info(failed.getCause()+" cought!");
		getFailureHandler().onAuthenticationFailure(request, response, failed);
	}


	private LoginUser getLoginUserFrom(HttpServletRequest request, HttpServletResponse response) {
		
		try {
			
			InputStream in = request.getInputStream();
			ObjectMapper mapper = new ObjectMapper();
			LoginUser loginUser =  mapper.readValue(in, LoginUser.class);
			return loginUser;
			
		} catch (IOException e) {
			
		}
			
		return null;
	}


}
