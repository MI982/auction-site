package auctionhouse.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import auctionhouse.controlleradvice.GlobalExceptionHandler;

@Component
public class FilterExceptionResolver extends OncePerRequestFilter{
	
	private static final Logger log = LoggerFactory.getLogger(FilterExceptionResolver.class);

	
	@Autowired
	private GlobalExceptionHandler exceptionHandler;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		try {
			filterChain.doFilter(request, response);
		} catch(Exception e) {
			log.info(e.getClass()+" cought!");
			log.info("Cause: "+e.getCause());
			exceptionHandler.handle(e, response);
		}
		
	}

}
