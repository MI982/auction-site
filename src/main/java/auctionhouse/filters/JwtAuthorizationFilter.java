package auctionhouse.filters;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import auctionhouse.models.UserDTO;
import auctionhouse.services.JwtService;
import auctionhouse.services.UserService;

@Component
public class JwtAuthorizationFilter extends OncePerRequestFilter{
	
	
	private static final Logger log = LoggerFactory.getLogger(JwtAuthorizationFilter.class);

	
	
	private static final String AUTHORIZATION = "Authorization";
	
	private static final String BEARER = "Bearer ";
	
	private static final String EMPTYSTRINGHOLDER = "";
	
	@Autowired
	private JwtService tokenService;
	
	@Autowired
	private UserService userService;

	@Override
	protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws ServletException, IOException {
		
		log.info("Attempting authorization!");
		
		String header = request.getHeader(AUTHORIZATION);
		
		if(Strings.isNotBlank(header) && header.startsWith(BEARER)) {
			
			String token = header.replace(BEARER, EMPTYSTRINGHOLDER);
			String username = tokenService.getUsername(token);
			
			log.info("Username: "+username);
			
			if(Strings.isNotBlank(username) && SecurityContextHolder.getContext().getAuthentication() == null) {
				
				UserDTO userDetails = userService.getUserForSecurityCheck(username);
				
				log.info("User service got user for security check!");
				
				if(userDetails.getIsActive()>0) {
					
					UsernamePasswordAuthenticationToken authToken = new UsernamePasswordAuthenticationToken(userDetails,
																											null,
																											userDetails.getAuthorities());
					authToken.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
					
					SecurityContextHolder.getContext().setAuthentication(authToken);
				}
			}
		}
		
		filterChain.doFilter(request, response);
		
	}

}
