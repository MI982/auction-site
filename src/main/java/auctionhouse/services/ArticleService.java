package auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import auctionhouse.models.Article;
import auctionhouse.repositories.ArticleRepository;

@Service
public class ArticleService {
	
	@Autowired
	private ArticleRepository repository;
	
	
	public Article save(Article article) {
		return repository.saveAndFlush(article);
	}

	public Article findById(Long id) {
		return repository.findById(id).get();
	
	}

}
