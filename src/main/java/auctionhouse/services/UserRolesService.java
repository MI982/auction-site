package auctionhouse.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import auctionhouse.models.UserRoles;
import auctionhouse.repositories.UserRolesRepository;

@Service
public class UserRolesService {
	
	@Autowired
	private UserRolesRepository repository;
	
	@Transactional
	public UserRoles save(UserRoles userRoles) {
		return repository.saveAndFlush(userRoles);
	}
	
	@Transactional
	public List<UserRoles> findRolesByUsername(Integer userId){
		return repository.findAllByUserId(userId);
	}
	

}
