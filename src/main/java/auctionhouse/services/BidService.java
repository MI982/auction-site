package auctionhouse.services;

import java.time.Instant;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import auctionhouse.models.Auction;
import auctionhouse.models.Bid;
import auctionhouse.models.Bid_;
import auctionhouse.models.User;
import auctionhouse.repositories.BidRepository;
import auctionhouse.specifications.BidSpecifications;

@Service
public class BidService {

	
	@Autowired
	private BidRepository bidRepository;
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private AuctionService auctionService;
	
	public Boolean placeBid(Long auctionId, String username, Integer bidValue) {
		
		User user = userService.findByUsername(username);
		Auction auction = auctionService.findEntityById(auctionId);
		
		if(user == null || auction == null) return false;
		Bid bid = new Bid();
		
		bid.setUser(user);
		bid.setAuction(auction);
		bid.setBidValue(bidValue);
		
		Long currentTime = null;
		
		if((currentTime = Instant.now().getEpochSecond()) < auction.getEndOn()) {
			bid.setBidTime(currentTime);
			bid = bidRepository.saveAndFlush(bid);
		}
		
		if (bid == null) return false; else return true;
	}
	
	public List<Bid> getAllBids(){
		return bidRepository.findAll();
	}

	public Bid save(Bid bid) {
		return bidRepository.saveAndFlush(bid);
	}
	
	public Bid find(Bid bid) {
		Optional<Bid> optionalBid = bidRepository.findOne(Example.of(bid));
		if(optionalBid.isPresent()) return optionalBid.get();
		return null;
	}
	
	public Page<Bid> getAllUserBids(Long userId, Integer pageNumber){
		
		if(userId == null) return null;
		
		Integer page = pageNumber == null || pageNumber <= 0 ? 0: pageNumber -1; 
		Integer size = 20;
		Sort sort = Sort.by(Direction.DESC, Bid_.BID_TIME);
		
		return bidRepository.findByUserId(userId, Specification.where(BidSpecifications.hasNotExpired()), PageRequest.of(page, size, sort));
		
	}
}
