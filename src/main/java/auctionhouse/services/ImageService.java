package auctionhouse.services;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import auctionhouse.models.Images;
import auctionhouse.repositories.ImagesRepository;

@Service
public class ImageService {
	
	@Autowired
	private ImagesRepository repository;
	
	public String saveToFile(MultipartFile image) throws IOException {
		
		Path srcMainDir = Paths.get("..");
		Path resDir = Paths.get(srcMainDir.toAbsolutePath().toString(), "/resources");
		Path imageDir = Paths.get(resDir.toAbsolutePath().toString(), "/images");
		
		if (Files.notExists(imageDir, new LinkOption[]{ LinkOption.NOFOLLOW_LINKS})) Files.createDirectory(imageDir);
		
		StringBuilder savedImagePath = new StringBuilder();
		
		savedImagePath.append(imageDir.toAbsolutePath().toString())
						.append("/")
						.append(UUID.randomUUID().toString().replace("-", ""))
						.append(".")
						.append(getExtension(image));
		
		Path finalSavePath = Paths.get(savedImagePath.toString());
		
		byte[] imageBytes = image.getBytes();
		
		Files.write(finalSavePath, imageBytes);
		
		return finalSavePath.toAbsolutePath().toString();
	}
	
	public byte[] getFromFile(Long imageId) throws IOException {
		
		return Files.readAllBytes(Paths.get(repository.findById(imageId).get().getPath()));
	}
	
	private String getExtension(MultipartFile image) {
		
		StringBuilder extension = new StringBuilder();
		
		String imageName = image.getOriginalFilename().toString();
		
		if(!imageName.equals("")) extension.append(imageName.substring(imageName.lastIndexOf(".")+1, imageName.length()));
		
		return extension.toString();
	}
	
	public Long saveToDb(String imageName, String imagePath) {
		
		return repository.saveAndFlush(new Images(imagePath,imageName, null)).getId();
	}
	
	public Images save(Images image) {
		return repository.saveAndFlush(image);
	}

	public Boolean saveAll(List<Images> imageList) {
		imageList = repository.saveAll(imageList);
		
		for(Images image : imageList) {
			if(image == null) return false;
		}
		
		return true;
	}

}
