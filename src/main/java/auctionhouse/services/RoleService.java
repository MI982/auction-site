package auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import auctionhouse.models.Role;
import auctionhouse.repositories.RoleRepository;

@Service
public class RoleService {
	
	@Autowired
	private RoleRepository repository;

	public Role getDefaultRole() {
		return repository.findByRole("ROLE_USER");
	}

}
