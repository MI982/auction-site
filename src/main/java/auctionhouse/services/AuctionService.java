package auctionhouse.services;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.apache.logging.log4j.util.Strings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.domain.Specifications;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import auctionhouse.models.Auction;
import auctionhouse.models.AuctionDTO;
import auctionhouse.models.Auction_;
import auctionhouse.models.SearchForm;
import auctionhouse.repositories.AuctionRepository;
import auctionhouse.specifications.AuctionSpecifications;

@Service
public class AuctionService {
	
	@Autowired
	private AuctionRepository auctionRepository;
	
	public List<Auction> getAll(){
		return auctionRepository.findAll();
	}

	public Auction save(Auction auction) {
		return auctionRepository.saveAndFlush(auction);
	}
	
	public AuctionDTO findById(Long id) {
		Optional<Auction> auction =  auctionRepository.findById(id);
		if (auction.isPresent()) return new AuctionDTO(auction.get());
		return null;
	}
	
	public Auction findEntityById(Long Id) {
		return auctionRepository.findById(Id).get();
	}
	
	
	public Page<AuctionDTO> getWithPaging(Integer pageNumber, Integer pageSize, Long pageTotal) {
		
		int number = pageNumber == null? 0: pageNumber - 1;
		
		int size = pageSize == null || pageSize < 20 ? 20: pageSize;
		
		long total = pageTotal == null? auctionRepository.count(): pageTotal;
		
		total = total % size > 0? total+1: total;
		
		Page<Auction> auctionList = auctionRepository.findAll(PageRequest.of(number, size, Sort.by("createdOn").descending()));
		
//		Page<AuctionDTO> auctionView = auctionList.map(new Converter<Auction, AuctionDTO>(){
//			
//			public AuctionDTO convert(Auction auction) {
//				return new AuctionDTO(auction);
//			}
//		});
		
		Page<AuctionDTO> auctionDtoList = auctionList.map(auction -> new AuctionDTO(auction));
		
		return auctionDtoList;
		
	}
	
	
	public Page<AuctionDTO> searchAuctions(SearchForm searchForm){
		
		Integer pageNumber = searchForm.getCurrentPageNumber() == null ? 0: searchForm.getCurrentPageNumber() <= 0? 0: searchForm.getCurrentPageNumber() - 1;
		Integer pageSize = searchForm.getResultsPerPage() == null || searchForm.getResultsPerPage() < 20 ? 20: searchForm.getResultsPerPage();
		String sortBy = searchForm.getSortBy();
		String direction = searchForm.getSortingDirection();
		String keyword = prepare(searchForm.getKeyword());
		
		sortBy = Strings.isNotBlank(sortBy) && ifFieldExists(sortBy) ? sortBy: Auction_.STARTING_PRICE;
		Direction sortDirection = Strings.isNotBlank(direction) && direction.equalsIgnoreCase("asc") ? Direction.ASC: Direction.DESC;
		
		Sort sort = Sort.by(sortDirection, sortBy);
		
		
			
			return auctionRepository.findAll(Specification
										.where(AuctionSpecifications.nameLike(keyword))
										.or(AuctionSpecifications.categoryLike(searchForm.getCategory()))
										.or(AuctionSpecifications.descriptionLike(keyword))
										.or(AuctionSpecifications.priceLessThan(searchForm.getMaxPrice()))
										.or(AuctionSpecifications.priceGreaterThan(searchForm.getMinPrice()))
										.and(AuctionSpecifications.hasNotExpired()) 
										,PageRequest.of(pageNumber, pageSize, sort)).
					map(auction -> new AuctionDTO(auction));
		
	}
	
//	public List<AuctionDTO> findByUserId(Long userId){
//		
//		return auctionRepository.findById(userId).map(auction -> new AuctionDTO(auction));
//		
//	}
	
	private Boolean ifFieldExists(String inputFieldName) {
		
		Boolean exists = false;
		
		try {
			
			for(Field field : Arrays.asList(Auction_.class.getFields())) if(field.getName().equals(inputFieldName)) exists = true;
				
		} catch (SecurityException e) {
			//?!?
		}
		 return exists;
	}
	
	private String prepare(String s) {
		
		if(Strings.isBlank(s)) return "%";
		
		StringBuilder keyword = new StringBuilder();
		keyword.append("%");
		keyword.append(s.toLowerCase());
		keyword.append("%");
		
		return keyword.toString();
	}

	public Page<Auction> findByUserIdAndPagination(Long id, Integer pageRequested) {
		
		if(id == null) return null;
		Integer page = pageRequested == null || pageRequested <= 0 ? 0: pageRequested - 1;  
		Integer size = 20;
		Sort sort = Sort.by(Direction.DESC, Auction_.CREATED_ON);
		
		return auctionRepository.findByUserId(id, Specification.where(AuctionSpecifications.hasNotExpired()), PageRequest.of(page, size, sort));
	}
	
	

}
