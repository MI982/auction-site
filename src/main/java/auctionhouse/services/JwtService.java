package auctionhouse.services;

import java.time.Duration;
import java.time.Instant;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import auctionhouse.models.User;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

@Service
public class JwtService {
	
	private int tokenDuration = 8;
	
	private String tokenSecret = "thisisnotagoodsolution";
	private String authorities = "Authorities";
	
//	public String createJwToken(User user) {
//		return createJwToken(user.getUsername());
//	}
	
	public String createJwToken(UserDetails uDetails) {
		//HashMap<String, Object> userDetailsForToken = new HashMap<>();
		
		return createJwToken(uDetails.getUsername(), uDetails.getAuthorities());
	}
	
	private String createJwToken(String username, Collection<? extends GrantedAuthority> grantedAuthorities) {
		return Jwts.builder()
				 .setSubject(username)
				 .claim(authorities, grantedAuthorities)
				 .setIssuedAt(new Date(System.currentTimeMillis()))
				 .setExpiration(Date.from(Instant.now().plus(Duration.ofHours(tokenDuration))))
				 .signWith(SignatureAlgorithm.HS256, tokenSecret)
				 .compact();
	}
	
	public Boolean hasTokenExpired(String token) {
		return getAllClaims(token).getExpiration().after(Date.from(Instant.now()));
	}
	
	public String getUsername(String token) {
		return getAllClaims(token).getSubject();
	}
	
	public Collection<? extends GrantedAuthority> getGrantedAuthorities(String token){
		return (Collection<? extends GrantedAuthority>)getAllClaims(token).get("Authorities");
	}
	
	private Claims getAllClaims(String token) {
		return Jwts.parser().setSigningKey(tokenSecret).parseClaimsJws(token).getBody();
	}

	public boolean isTokenValid(String token, String username) {
		return username.equals(getUsername(token)) && hasTokenExpired(token);
	}
	
	

}
