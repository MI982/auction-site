package auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import auctionhouse.models.User;
import auctionhouse.models.UserDTO;

@Service
public class CustomDetailsService implements UserDetailsService{
	
	@Autowired
	private UserService userService;
	

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		UserDTO user = userService.getUserForSecurityCheck(username);
		
		if(user == null) throw new UsernameNotFoundException("Username "+ username + " not found!");
		
		return user;
		
	}

}
