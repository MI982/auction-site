package auctionhouse.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import auctionhouse.models.Category;
import auctionhouse.repositories.CategoryRepository;

@Service
public class CategoryService {
	
	@Autowired
	private CategoryRepository repository;
	
	public Category getByName(String name) {
		return repository.findByName(name);
	}

	public Category createNewCategory(String categoryName) {
		Category category = new Category();
		category.setName(categoryName);
		return repository.saveAndFlush(category);
	}

}
