package auctionhouse.services;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import auctionhouse.models.User;
import auctionhouse.models.UserDTO;
import auctionhouse.repositories.UserRepository;
import auctionhouse.specifications.UserSpecifications;

@Service
public class UserService {
	
	
	private static final Logger log = LoggerFactory.getLogger(UserService.class);

	
	@Autowired
	private UserRepository userRepo;
	
	@Transactional
	public List<User> getAllUsers() {
		return userRepo.findAll();
	}
	
	@Transactional
	public User register(User user) {
		return userRepo.saveAndFlush(user);
	}
	
	@Transactional
	public User findByUsername(String username) {
		return userRepo.findByUsername(username);
	}
	
	public UserDTO getUserForSecurityCheck(String username) throws UsernameNotFoundException {
		Optional<User> user = userRepo.findOne(Specification.where(UserSpecifications.usernameEquals(username)));
		return user.map(UserDTO::new).orElseThrow(() -> new UsernameNotFoundException("Username "+username+" not found!"));
	}

}
