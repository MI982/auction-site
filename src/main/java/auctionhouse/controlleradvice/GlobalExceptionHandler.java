package auctionhouse.controlleradvice;

import java.io.IOException;
import java.io.OutputStream;
import java.io.Writer;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.InternalAuthenticationServiceException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.fasterxml.jackson.core.JsonGenerationException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import auctionhouse.customexceptions.LoginUserException;
import auctionhouse.responseerrormodels.BadCredentialsErrorResponse;
import auctionhouse.responseerrormodels.ErrorResponse;
import auctionhouse.responseerrormodels.LoginUserErrorResponse;
import auctionhouse.responseerrormodels.MethodArgumentNotValidResponse;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureException;

@RestControllerAdvice
public class GlobalExceptionHandler {
	
	private static final Logger log = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	private String dateTimePattern = "HH:mm:ss dd-MM-uuuu";
	private String contentType = "application/json";

	
	@ExceptionHandler
	public ResponseEntity<?> handle(Exception exception, HttpServletResponse response) {
		
		if(exception instanceof BadCredentialsException) { 
			try {
				return handleBadCredentials((BadCredentialsException)exception, response);
			} catch (IOException e) {
				return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).contentType(MediaType.APPLICATION_JSON_UTF8).body(e.getMessage());
			}
		}
		
		if(exception instanceof MethodArgumentNotValidException) {
			return handleMethodArgumentNotValid((MethodArgumentNotValidException)exception, response);
		}
		
		return null;
		
	}
	
	
	
	
	private ResponseEntity<?> handleMethodArgumentNotValid(MethodArgumentNotValidException exception, HttpServletResponse response){
		MethodArgumentNotValidResponse errorResponse = new MethodArgumentNotValidResponse();
		BindingResult bindingResult = exception.getBindingResult();
		
		HashMap<String, String> errorHolder = getHashMapFromBindingResult(bindingResult);
		
		errorResponse.setErrors(errorHolder);
		errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
		errorResponse.setTimestamp(LocalDateTime.now().format(DateTimeFormatter.ofPattern(dateTimePattern)));
		return ResponseEntity.badRequest().contentType(MediaType.APPLICATION_JSON).body(errorResponse);
	}
	
	
	private ResponseEntity<?> handleBadCredentials(BadCredentialsException exception, HttpServletResponse response) throws IOException {
		
		ErrorResponse errorResponse;
		ObjectMapper mapper = new ObjectMapper();
		
		if (exception.getCause() != null && exception.getCause() instanceof LoginUserException) {
			BindingResult bindingResult = ((LoginUserException) exception.getCause()).getBindingResult();
			errorResponse = mapper.convertValue(getHashMapFromBindingResult(bindingResult), LoginUserErrorResponse.class);
			
		}else {
			errorResponse = new BadCredentialsErrorResponse();
			errorResponse.setMessage(exception.getMessage());
		}
		
		errorResponse.setStatus(HttpStatus.BAD_REQUEST.value());
		errorResponse.setTimestamp(LocalDateTime.now().format(DateTimeFormatter.ofPattern(dateTimePattern)));
		response.setStatus(HttpStatus.BAD_REQUEST.value());
		response.setContentType(contentType);
		mapper.writeValue(response.getOutputStream(), errorResponse);
		return null;
	}
	
	
	
	
	
	private HashMap<String, String> getHashMapFromBindingResult(BindingResult bindingResult){
		HashMap<String, String> errorHolder = new HashMap<>();
		bindingResult.getAllErrors().forEach(error -> {
			String fieldName = ((FieldError)error).getField();
			String errorMessage = error.getDefaultMessage();
			errorHolder.put(fieldName, errorMessage);
		});
		return errorHolder;
	}

}
