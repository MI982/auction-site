package auctionhouse.responseerrormodels;

public class BadCredentialsErrorResponse extends ErrorResponse {
	
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	

}
