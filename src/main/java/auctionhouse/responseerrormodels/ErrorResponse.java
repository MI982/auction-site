package auctionhouse.responseerrormodels;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorResponse {
	
	private int status;
	private String message;
	private String timestamp;
	
	public ErrorResponse() {}
	
	public ErrorResponse(int status, String message, String timestamp) {
		this.status = status;
		this.message = message;
		this.timestamp = timestamp;
	}
	
	public static ErrorResponse createNew() {
		return new ErrorResponse();
	}
	
	public ErrorResponse addStatus(int status) {
		this.status = status;
		return this;
	}
	
	public ErrorResponse addMessage(String message) {
		this.message = message;
		return this;
	}
	
	public ErrorResponse addTimestamp(String timestamp) {
		this.timestamp = timestamp;
		return this;
	}

}
