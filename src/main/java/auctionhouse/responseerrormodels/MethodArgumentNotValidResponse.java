package auctionhouse.responseerrormodels;

import java.util.HashMap;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MethodArgumentNotValidResponse extends ErrorResponse{
	
	private HashMap<String, String> errors;
}
