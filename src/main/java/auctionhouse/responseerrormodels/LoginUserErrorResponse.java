package auctionhouse.responseerrormodels;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginUserErrorResponse extends ErrorResponse{
	
	private String username;
	
	private String password;

}
