insert into role (id, role) values (1, 'USER'), (2, 'ADMIN');
insert into category (id, name) values (1, 'Books'), (2, 'Comics'),(3, 'Board Games'),(4, 'Shoes'),(5, 'Toys'),(6, 'Phones'),(7, 'Cars');
insert into article (id, description, name) values (1, 'It\'s a book Jim', 'Star Trek'), (2, 'It\'s dead Jim', 'Star Wars'), (3, 'For the Emprah!', 'Forbiden Stars');
insert into user(id, first_name, is_active, last_name, location, password, phone_number, user_name) values (1, 'Kit', 1, 'Fisto', 'Glee Anselm', '123', '+23564645232', 'tentaclefreak101');
insert into user_roles (user_id, role_id) values (1, 2);
insert into auction (id, created_on, end_on, starting_price, article_id, category_id, user_id) values (1, 1234, 4321, 1000000, 2, 2, 1);
insert into bid (bid_price, bid_time, auction_id, user_id) values (1000000000, 4320, 1, 1);
